import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() => runApp(const XylophoneApp());

class XylophoneApp extends StatelessWidget {
  const XylophoneApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: const Center(
              child: Text(
            'Xylophone',
          )),
          backgroundColor: Colors.teal[300],
          foregroundColor: Colors.black,
        ),
        body: const SafeArea(
          child: Center(
            child: Xylophone(),
          ),
        ),
      ),
    );
  }
}

class Xylophone extends StatelessWidget {
  const Xylophone({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Bar(
          color: Colors.red,
          note: 1,
        ),
        Bar(
          color: Colors.orange,
          note: 2,
        ),
        Bar(
          color: Colors.yellow,
          note: 3,
        ),
        Bar(
          color: Colors.green,
          note: 4,
        ),
        Bar(
          color: Colors.blue,
          note: 5,
        ),
        Bar(
          color: Colors.indigo,
          note: 6,
        ),
        Bar(
          color: Colors.purple,
          note: 7,
        ),
      ],
    );
  }
}

class Bar extends StatelessWidget {
  Bar({
    required this.color,
    required this.note,
    Key? key,
  }) : super(key: key);

  final AudioPlayer player = AudioPlayer();
  final int? note;
  final MaterialColor color;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        splashColor: color[400],
        onTapDown: (tapDown) {
          player.setSourceAsset('note$note.wav');
          player.resume();
          debugPrint('clicked 🎹');
        },
        onTapUp: (tapUp) {
          player.stop();
        },
        child: Container(color: color.withAlpha(200)),
      ),
    );
  }
}
